let collection = [];

// Write the queue functions below.
function print(){
    return collection;
}

function enqueue(element) {
// Add element
if(collection.length === 0){
    collection[0] = element;
}else {
    collection[collection.length] = element;
}
    return collection;
}

function dequeue() {
// Remove element
    for(let i = 0;i < collection.length;i++ ){
        if(i === collection.length){
            collection.length = collection.length - 1;
        } else{
            collection[i] = collection[i+1]
        }
    }
    collection.length--
    return collection;

    // collection[0]
}

function front() {
// get first element
    return collection[0];
}

function size() {
// Get size
return collection.length;
}

function isEmpty() {
// Check if empty
return (collection.length === 0)
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};